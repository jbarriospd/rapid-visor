import _debounce from 'lodash-es/debounce';

import { event as d3_event } from 'd3-selection';

import { t } from '../util/locale';
import { svgIcon } from '../svg/index';


export function uiNotice(context) {

    return function(selection) {
        var div = selection
            .append('div')
            .attr('class', 'notice');

    
        button
            .call(svgIcon('#iD-icon-plus', 'pre-text'))
            .append('span')
            .attr('class', 'label')
            .text(t('zoom_in_edit'));


        function disableTooHigh() {
            var canEdit = context.map().zoom() >= context.minEditableZoom();
            div.style('display', canEdit ? 'none' : 'block');
        }

        context.map()
            .on('move.notice', _debounce(disableTooHigh, 500));

        disableTooHigh();
    };
}
